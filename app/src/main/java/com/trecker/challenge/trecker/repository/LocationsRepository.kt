package com.trecker.challenge.trecker.repository

import com.trecker.challenge.trecker.model.Location
import io.reactivex.Single

interface LocationsRepository {

    fun getDriverLocations(): Single<List<Location>>

}