package com.trecker.challenge.trecker.model

data class Driver(
        val id: String,
        val name: String,
        val active: Boolean
)
