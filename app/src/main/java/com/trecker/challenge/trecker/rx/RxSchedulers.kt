package com.trecker.challenge.trecker.rx

import io.reactivex.Scheduler

class RxSchedulers(val io: Scheduler, val androidMainThread: Scheduler)
