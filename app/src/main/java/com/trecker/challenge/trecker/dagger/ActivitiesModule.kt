package com.trecker.challenge.trecker.dagger

import android.arch.lifecycle.ViewModelProvider
import com.trecker.challenge.trecker.ui.activity.MapsActivity
import com.trecker.challenge.trecker.ui.activity.MapsActivitySubModule
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivitiesModule {

    @ContributesAndroidInjector(modules = [(MapsActivitySubModule::class)])
    abstract fun bindsMapsActivity(): MapsActivity

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

}