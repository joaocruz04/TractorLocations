package com.trecker.challenge.trecker.network

import com.trecker.challenge.trecker.model.Driver
import com.trecker.challenge.trecker.model.Location
import io.reactivex.Single
import retrofit2.http.GET

interface TractorAPI {

    @GET("positions.json")
    fun driverLocations(): Single<List<Location>>

    @GET("drivers.json")
    fun drivers(): Single<List<Driver>>

}