package com.trecker.challenge.trecker.ui.activity

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.widget.Toast
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLngBounds
import com.google.maps.android.clustering.ClusterManager
import com.trecker.challenge.trecker.R
import com.trecker.challenge.trecker.model.DriverTractor
import com.trecker.challenge.trecker.ui.view.LocationMapItem


class MapsActivity : BaseActivity<MapsActivityViewModel>(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    private lateinit var clusterManager: ClusterManager<LocationMapItem>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        initFragment()
        observeLocations()
    }

    private fun initFragment() {
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        setupMap(googleMap)
        viewModel.loadLocations()
    }

    private fun setupMap(googleMap: GoogleMap) {
        mMap = googleMap
        clusterManager = ClusterManager(this, mMap)
        mMap.setOnCameraIdleListener(clusterManager)
        mMap.setOnMarkerClickListener(clusterManager)
    }

    private fun observeLocations() {
        viewModel.driverTractorLiveData.observe(this, Observer {
            it?.let { setLocations(it) } ?: showError()
        })
    }

    private fun showError() {
        Toast.makeText(this@MapsActivity, "Error getting locations!", Toast.LENGTH_SHORT).show()
    }

    private fun setLocations(locations: List<DriverTractor>) {
        locations.takeIf { it.isNotEmpty() }
                ?.let {
                    clusterManager.clearItems()
                    val builder = LatLngBounds.Builder()
                    it.map {
                        LocationMapItem(it.location.latitude, it.location.longitude, it.driver.name, it.driver.id)
                    }.forEach {
                        clusterManager.addItem(it)
                        builder.include(it.position)
                    }
                    val bounds = builder.build()
                    mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100))
                }
    }

    override fun constructViewModel(): Class<MapsActivityViewModel> {
        return MapsActivityViewModel::class.java
    }
}
