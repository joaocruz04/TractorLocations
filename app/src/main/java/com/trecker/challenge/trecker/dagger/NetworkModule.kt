package com.trecker.challenge.trecker.dagger

import com.trecker.challenge.trecker.network.TractorAPI
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton


@Module
class NetworkModule {

    @Provides
    @Singleton
    fun provideTractorAPI(): TractorAPI {
        val retrofit = Retrofit.Builder()
                .baseUrl("http://coding-challenge-assets.s3.eu-central-1.amazonaws.com")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()

        return retrofit.create(TractorAPI::class.java)
    }

}