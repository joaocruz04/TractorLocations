package com.trecker.challenge.trecker.ui.view

import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.clustering.ClusterItem

class LocationMapItem(
        private val lat: Double,
        private val lon: Double,
        private val itemTitle: String,
        private val itemSnippet: String
) : ClusterItem {

    override fun getSnippet() = itemSnippet

    override fun getTitle() = itemTitle

    override fun getPosition() = LatLng(lat, lon)

}