package com.trecker.challenge.trecker.ui.activity

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.trecker.challenge.trecker.model.DriverTractor
import com.trecker.challenge.trecker.usecase.location.GetLocationsUseCase
import io.reactivex.observers.DisposableSingleObserver
import javax.inject.Inject

class MapsActivityViewModel @Inject constructor(
        private val dataUseCase: GetLocationsUseCase
) : ViewModel() {

    val driverTractorLiveData = MutableLiveData<List<DriverTractor>>()

    fun loadLocations() {
        dataUseCase.execute(DriverTractorSubscriber())
    }

    inner class DriverTractorSubscriber : DisposableSingleObserver<List<DriverTractor>>() {
        override fun onSuccess(locations: List<DriverTractor>) {
            driverTractorLiveData.postValue(locations)
        }

        override fun onError(e: Throwable) {
            driverTractorLiveData.postValue(null)
        }
    }

    public override fun onCleared() {
        super.onCleared()
        dataUseCase.dispose()
    }
}