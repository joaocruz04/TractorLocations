package com.trecker.challenge.trecker

import com.trecker.challenge.trecker.dagger.DaggerAppComponent
import dagger.android.support.DaggerApplication

class App : DaggerApplication() {

    override fun applicationInjector() = DaggerAppComponent.builder()
            .application(this)
            .build()

}