package com.trecker.challenge.trecker.usecase.location

import com.trecker.challenge.trecker.model.Driver
import com.trecker.challenge.trecker.model.DriverTractor
import com.trecker.challenge.trecker.model.Location
import com.trecker.challenge.trecker.repository.DriversRepository
import com.trecker.challenge.trecker.repository.LocationsRepository
import com.trecker.challenge.trecker.rx.RxSchedulers
import com.trecker.challenge.trecker.usecase.UseCaseSingle
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import javax.inject.Inject

open class GetLocationsUseCase @Inject constructor(
        private val driversRepository: DriversRepository,
        private val locationsRepository: LocationsRepository,
        rxSchedulers: RxSchedulers) : UseCaseSingle<List<DriverTractor>>(rxSchedulers) {

    override fun buildUseCaseObservable(): Single<List<DriverTractor>> {
        val driversObservable = driversRepository.getDrivers()
        val locationsObservable = locationsRepository.getDriverLocations()

        return driversObservable.zipWith(locationsObservable,
                BiFunction { drivers: List<Driver>, locations: List<Location> ->
                    locations.mapNotNull {
                        val location = it
                        val driver = getDriver(it.driver_id, drivers)
                        driver?.let { DriverTractor(it, location) }
                    }.toList()
                })
    }

    private fun getDriver(driverId: String, driverList: List<Driver>) = driverList.firstOrNull { it.id == driverId }

}