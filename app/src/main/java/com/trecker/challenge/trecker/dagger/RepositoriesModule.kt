package com.trecker.challenge.trecker.dagger

import com.trecker.challenge.trecker.repository.DriversRepository
import com.trecker.challenge.trecker.repository.DriversRepositoryImpl
import com.trecker.challenge.trecker.repository.LocationsRepository
import com.trecker.challenge.trecker.repository.LocationsRepositoryImpl
import dagger.Binds
import dagger.Module

@Module
abstract class RepositoriesModule {

    @Binds
    abstract fun bindDriversRepository(driversRepository: DriversRepositoryImpl): DriversRepository

    @Binds
    abstract fun bindLocationsRepository(locationsRepository: LocationsRepositoryImpl): LocationsRepository

}