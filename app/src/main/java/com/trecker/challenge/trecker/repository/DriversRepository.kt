package com.trecker.challenge.trecker.repository

import com.trecker.challenge.trecker.model.Driver
import io.reactivex.Single

interface DriversRepository {

    fun getDrivers(): Single<List<Driver>>

}