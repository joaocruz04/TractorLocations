package com.trecker.challenge

import com.trecker.challenge.trecker.network.TractorAPI
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.assertj.core.api.Assertions.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.InputStreamReader
import java.nio.charset.StandardCharsets


@RunWith(RobolectricTestRunner::class)
class ApiIntegrationTest {

    companion object {

        private const val DRIVERS_RESULTS_FILENAME = "drivers_results.json"
        private const val LOCATIONS_RESULTS_FILENAME = "locations_results.json"

    }

    private val mockWebServer = MockWebServer()

    private lateinit var testedClass: TractorAPI

    @Before
    fun setUp() {
        mockWebServer.start()
        testedClass = provideTractorAPI()
    }


    @Test
    fun `When requesting drivers list, then a list of drivers is returned`() {
        mockWebServer.enqueue(MockResponse().setBody(provideJsonFromFile(DRIVERS_RESULTS_FILENAME)))

        val response = testedClass.drivers().blockingGet()

        assertThat(response).isNotNull
        assertThat(response.size).isEqualTo(8)
        assertThat(response[0].name).isEqualTo("Amelie")
    }

    @Test
    fun `When requesting locations list, then a list of locations is returned`() {
        mockWebServer.enqueue(MockResponse().setBody(provideJsonFromFile(LOCATIONS_RESULTS_FILENAME)))

        val response = testedClass.driverLocations().blockingGet()

        assertThat(response).isNotNull
        assertThat(response.size).isEqualTo(9)
        assertThat(response[0].driver_id).isEqualTo("44c33650-90d3-4ea5-9172-494868c96fc1")
    }


    @After
    fun tearDown() {
        mockWebServer.shutdown()
    }

    /* HELPERS */

    private fun provideTractorAPI(): TractorAPI {
        val retrofit = Retrofit.Builder()
                .baseUrl(mockWebServer.url("coding-challenge-assets.s3.eu-central-1.amazonaws.com/"))
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()

        return retrofit.create(TractorAPI::class.java)
    }

    private fun provideJsonFromFile(fileName: String): String {
        val resource = this::class.java.classLoader.getResource(fileName)
        val inputStream = resource.openStream()
        val stringBuilder = StringBuilder()
        val bufferedReader = InputStreamReader(inputStream, StandardCharsets.UTF_8)
        bufferedReader.use { stringBuilder.append(it.readText()) }
        return stringBuilder.toString()
    }
}