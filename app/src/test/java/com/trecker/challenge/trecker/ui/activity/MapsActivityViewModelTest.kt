package com.trecker.challenge.trecker.ui.activity

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.Observer
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.trecker.challenge.trecker.model.Driver
import com.trecker.challenge.trecker.model.DriverTractor
import com.trecker.challenge.trecker.model.Location
import com.trecker.challenge.trecker.usecase.location.GetLocationsUseCase
import org.junit.Before
import org.junit.Rule
import org.junit.Test


class MapsActivityViewModelTest {

    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()

    private lateinit var searchResultObserverMock: Observer<List<DriverTractor>>
    private lateinit var dataUseCaseMock: GetLocationsUseCase

    private lateinit var testedClass: MapsActivityViewModel
    private lateinit var testedSubscriber: MapsActivityViewModel.DriverTractorSubscriber

    @Before
    fun setUp() {
        dataUseCaseMock = mock()
        searchResultObserverMock = mock()
        testedClass = MapsActivityViewModel(dataUseCaseMock)
        testedClass.driverTractorLiveData.observeForever(searchResultObserverMock)
        testedSubscriber = testedClass.DriverTractorSubscriber()
    }

    @Test
    fun `When user loads locations, then locations use case is executed`() {
        testedClass.loadLocations()

        verify(dataUseCaseMock).execute(any())
    }

    @Test
    fun `When view model is destroyed, then use case is disposed`() {
        testedClass.onCleared()

        verify(dataUseCaseMock).dispose()
    }

    @Test
    fun `When service returns a search result, then ui gets notified with new repositories`() {
        val searchResult = provideListOfDriverTractor()
        testedSubscriber.onSuccess(searchResult)

        verify(searchResultObserverMock).onChanged(searchResult)
    }

    @Test
    fun `When service returns an error, then ui gets notified`() {
        testedSubscriber.onError(Throwable())

        verify(searchResultObserverMock).onChanged(null)
    }

    /* HELPERS */

    private fun provideListOfDriverTractor() = listOf(DriverTractor(
            Driver("id1", "name1", true),
            Location("id1", 1.0, 1.0, "timestamp")
    ))

}